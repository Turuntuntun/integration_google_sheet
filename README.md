# Инструкция по интеграции

1) Сделать таблицу доступной по ссылке.

2) Выбрать библиотеку google sheet api https://console.cloud.google.com/apis/library;

3) Добавить ключ https://console.cloud.google.com/apis/credentials
    - Кнопка Manage service accounts
    - Кнопка CREATE SERVICE ACCOUNT
    - Добавить аккаунт 
    - Зайти в созданный аккаунт и нажать keys
    - Кнопка add key , create new key, json.

4) Добавить файл в папку с приложением.

5) Проприсать путь до файла в 3 строке.


# Запросы 

Принимает на вход 3 параметра:

1) $_POST['spreadsheetId'] - id таблицы.
2) $_POST['sheet'] - Id листа. 
3) $_POST['data'] - json с массивом данных

