<?php

$googleAccountKeyFilePath = __DIR__ . '/plexiform-skill-355712-899f064eece0.json';

$values = json_decode($_POST['data'],true);

require_once __DIR__ . '/vendor/autoload.php';

// Путь к файлу ключа сервисного аккаунта

putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );

// Документация https://developers.google.com/sheets/api/
$client = new Google_Client();
$client->useApplicationDefaultCredentials();

// Области, к которым будет доступ
// https://developers.google.com/identity/protocols/googlescopes
$client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );

$service = new Google_Service_Sheets( $client );

// ID таблицы
$spreadsheetId = $_POST['spreadsheetId'];

$response = $service->spreadsheets->get($spreadsheetId);

$body    = new Google_Service_Sheets_ValueRange( [ 'values' => $values ] );

// valueInputOption - определяет способ интерпретации входных данных
// https://developers.google.com/sheets/api/reference/rest/v4/ValueInputOption
// RAW | USER_ENTERED
$options = array( 'valueInputOption' => 'RAW' );

$service->spreadsheets_values->update( $spreadsheetId, $_POST['sheet'], $body, $options );